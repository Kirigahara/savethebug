﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TheManager : MonoBehaviour
{
    public JsonMapManager _JsonManager;

    public int _SelectedMapID = 0;

    public int _UnlockTotal = 1;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(_JsonManager.gameObject);
        DontDestroyOnLoad(this.gameObject);

        _UnlockTotal = PlayerPrefs.GetInt("UnlockLevel", 1);

        LoadSceneHome();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LoadSceneHome()
    {
        SceneManager.LoadScene("Home");
    }

    public void LoadSceneMainGame()
    {
        SceneManager.LoadScene("MainGame");
    }
}
