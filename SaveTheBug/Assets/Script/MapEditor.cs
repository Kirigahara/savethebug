﻿//#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[ExecuteInEditMode]
public class MapEditor : MonoBehaviour
{
#if UNITY_EDITOR
    public bool _SelectToTemp = true;
    public bool _SetTempPathMode = true;

    [MenuItem("Command/Create Path From Nodes")]
    public static void CreatePathFromNodes()
    {
        Node startNode = null;
        Node endNode = null;

        for (int i = 0; i < Selection.gameObjects.Length; i++)
        {
            Node selectedNode = Selection.gameObjects[i].GetComponent<Node>();
            if (selectedNode != null)
            {
                if (selectedNode._StartNode == true)
                {
                    startNode = selectedNode;
                }

                if (selectedNode._EndNode == true)
                {
                    endNode = selectedNode;
                }
            }
        }

        if (startNode == null || endNode == null)
        {
            return;
        }

        //Path path = new Path(startNode);
        //Node currentNode = startNode;

        //for (int i = 0; i < Selection.gameObjects.Length; i++)
        //{
        //    Node selectedNode = Selection.gameObjects[i].GetComponent<Node>();
        //    if (selectedNode == null)
        //        continue;
        //    if (currentNode.CheckNeighbor( )
        //}

        for (int i = 0; i < Selection.gameObjects.Length; i++)
        {
            Node selectedNode = Selection.gameObjects[i].GetComponent<Node>();
            selectedNode._TempPath = true;
        }
    }

    [MenuItem("Command/Lock Node Top")]
    public static void LockNodeTop()
    {
        for (int i = 0; i < Selection.gameObjects.Length; i++)
        {
            Node selectedNode = Selection.gameObjects[i].GetComponent<Node>();
            selectedNode.LockTop();
        }
    }

    [MenuItem("Command/Lock Node Right")]
    public static void LockNodeRight()
    {
        for (int i = 0; i < Selection.gameObjects.Length; i++)
        {
            Node selectedNode = Selection.gameObjects[i].GetComponent<Node>();
            selectedNode.LockRight();
        }
    }

    [MenuItem("Command/Lock Node Down")]
    public static void LockNodeDown()
    {
        for (int i = 0; i < Selection.gameObjects.Length; i++)
        {
            Node selectedNode = Selection.gameObjects[i].GetComponent<Node>();
            selectedNode.LockDown();
        }
    }

    [MenuItem("Command/Lock Node Left")]
    public static void LockNodeLeft()
    {
        for (int i = 0; i < Selection.gameObjects.Length; i++)
        {
            Node selectedNode = Selection.gameObjects[i].GetComponent<Node>();
            selectedNode.LockLeft();
        }
    }

    [MenuItem("Command/Unlock Selected Node")]
    public static void UnlockSelectedNode()
    {
        for (int i = 0; i < Selection.gameObjects.Length; i++)
        {
            Node selectedNode = Selection.gameObjects[i].GetComponent<Node>();
            selectedNode.Unlock();
        }
    }

    [ExecuteInEditMode]
    public void Update()
    {
        if (_SelectToTemp == false)
            return;

        try
        {
            Node n = Selection.activeTransform.parent.GetComponent<Node>();
            if (n != null)
            {
                if (_SetTempPathMode == true)
                {
                    if (n._TempPath == false)
                    {
                        Debug.Log("Add Node ID: " + n._Id.ToString());
                        n.SelectionTempPath();
                    }
                    else
                    {
                        Debug.Log("Remote Node ID: " + n._Id.ToString());
                        n.UnSelectionTempPath();
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
#endif
}
//#endif
