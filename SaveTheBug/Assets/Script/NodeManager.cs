﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NodeManager : MonoBehaviour
{
    public JsonMapManager _JsonMapManager;
    public Navigator _Navigator;
    public TheManager _TheManager;

    public HintController _HintObj;
    public BugController _BugObj;
    public VitualJoystick _VirtualJoyStick;
    public GameObject _GateObj;
    
    [Header("Prefab")]
    public GameObject _PrefabNode;
    public GameObject _PrefabBug;
    public GameObject _PrefabGate;

    public List<Node> _ListNode = new List<Node>();

    public List<Node> _ListTempPathNode = new List<Node>();

    public List<Path> _ListPath = new List<Path>();

    public List<Path> _ListConfirmPath = new List<Path>();

    [SerializeField]
    private List<string> _ListEncodePath = new List<string>();

    public GameObject _NodeHolder;
    public int _Row = 10;
    public int _Column = 13;

    public int _CurrentPathID = 0;
    public int _DeletePathID = 0;

    public Node _StartNode;
    public Node _EndNode;

    public string _MapName = "Map1";
    public int _CurrentMapID = 0;

    public Path _HintPath;

    public float _TotalTime = 180; // second

    [Header("UI")]
    public Image _ImgProcessBar;

    public Text _TxtLenAmount;
    public Text _TxtMapID;

    public GameObject _PopupComplete;
    public GameObject _PopupPause;

    public DoorController _Door;

    public bool _PauseGame = false;
    public bool _EndGame = false;

    #region Editor

    [ContextMenu(nameof(GenerateNode))]
    public void GenerateNode()
    {
        float deltaX = 0.48f;
        float deltaY = 0.48f;
        
        float ypos = 0;

        for (int i = 0; i < _Column; i++)
        {
            float xpos = 0;
            for (int e = 0; e < _Row; e++)
            {
                GameObject g = GameObject.Instantiate(_PrefabNode, _NodeHolder.transform);
                g.transform.localPosition = new Vector2(xpos, ypos);
                Node n = g.GetComponent<Node>();
                n._NodeManager = this;
                n._Id = i;
                _ListNode.Add(n);

                xpos += deltaX;
            }
            ypos -= deltaY;
        }
    }
    [ContextMenu(nameof(ClearNode))]
    public void ClearNode()
    {
        for(int i=0;i<_ListNode.Count;i++)
        {
            DestroyImmediate(_ListNode[i].gameObject);
        }
        _ListNode.Clear();
    }

    [ContextMenu(nameof(SetNeighbor))]
    public void SetNeighbor()
    {
        for (int i = 0; i < _ListNode.Count; i++)
        {
            if (_ListNode[i]._Right == null)
            {
                if ((i + 1) % _Row != 0 && (i + 1) < _ListNode.Count)
                {
                    _ListNode[i]._Right = _ListNode[i + 1];
                    _ListNode[i + 1]._Left = _ListNode[i];
                }
            }

            if (_ListNode[i]._Left == null)
            {
                if ((i - 1) >= 0 && i % _Row != 0)
                {
                    _ListNode[i]._Left = _ListNode[i - 1];
                    _ListNode[i - 1]._Right = _ListNode[i];
                }
            }

            if (_ListNode[i]._Top == null)
            {
                if ((i - _Column) >= 0)
                {
                    _ListNode[i]._Top = _ListNode[i - _Row];
                    _ListNode[i - _Row]._Down = _ListNode[i];
                }
            }

            if(_ListNode[i]._Down == null)
            {
                if((i+_Column)<_ListNode.Count)
                {
                    _ListNode[i]._Down = _ListNode[i + _Row];
                    _ListNode[i + _Row]._Top = _ListNode[i];
                }
            }
        }
    }

    [ContextMenu(nameof(ResetNode))]
    public void ResetNode()
    {
        for(int i=0;i<_ListNode.Count;i++)
        {
            _ListNode[i].ClearNode();
        }
    }

    [ContextMenu(nameof(HideMain))]
    public void HideMain()
    {
        for (int i = 0; i < _ListNode.Count; i++)
        {
            _ListNode[i].HideMain();
        }
    }
    [ContextMenu(nameof(ShowMain))]
    public void ShowMain()
    {
        for (int i = 0; i < _ListNode.Count; i++)
        {
            _ListNode[i].ShowMain();
        }
    }

    [ContextMenu(nameof(NormalizeNode))]
    public void NormalizeNode()
    {
        for(int i=0;i<_ListNode.Count;i++)
        {
            _ListNode[i].Unlock();
        }

        for (int i = 0; i < _ListNode.Count; i++)
        {
            if (_ListNode[i]._Top == null)
            {
                _ListNode[i].LockTop();
            }
            if (_ListNode[i]._Right == null)
            {
                _ListNode[i].LockRight();
            }
            if (_ListNode[i]._Down == null)
            {
                _ListNode[i].LockDown();
            }
            if (_ListNode[i]._Left == null)
            {
                _ListNode[i].LockLeft();
            }
        }
    }

    [ContextMenu(nameof(SetNodeID))]
    public void SetNodeID()
    {
        for(int i=0;i<_ListNode.Count;i++)
        {
            _ListNode[i]._Id = i;
            _ListNode[i].name = "Node_" + i.ToString();
        }
    }

    [ContextMenu(nameof(AddTempPath))]
    public void AddTempPath()
    {
        if (_ListPath.Count > 0)
        {
            for (int i = 0; i < _ListNode.Count; i++)
            {
                _ListNode[i].ResetTemp();
            }
        }

        int id = _ListPath.Count;

        _CurrentPathID = id;

        Path p = new Path();
        p._Id = id;
        p._NodeManager = this;

        _ListPath.Add(p);
        _ListEncodePath.Add(p.ToString());
    }

    [ContextMenu(nameof(DeletePathOnID))]
    public void DeletePathOnID()
    {
        if(_DeletePathID<_ListPath.Count)
        {
            _ListPath.RemoveAt(_DeletePathID);
            _ListEncodePath.RemoveAt(_DeletePathID);
        }
    }
    [ContextMenu(nameof(NormalizePath))]
    public void NormalizePath()
    {
        _ListPath[_CurrentPathID].Decode();
    }
    [ContextMenu(nameof(ResetNodesOnTemp))]
    public void ResetNodesOnTemp()
    {
        for(int i=0;i<_ListNode.Count;i++)
        {
            _ListNode[i].ResetNode();
        }
    }
    [ContextMenu(nameof(RemoveTempVisualization))]
    public void RemoveTempVisualization()
    {
        for (int i = 0; i < _ListNode.Count; i++)
        {
            _ListNode[i].ResetTemp();
        }
    }

    [ContextMenu(nameof(RandomMap))]
    public void RandomMap()
    {
        for (int i = 0; i < _ListNode.Count; i++)
        {
            _ListNode[i].RandomLock();
        }
    }

    [ContextMenu(nameof(ExportMapJson))]
    public void ExportMapJson()
    {
        ListNodeEncode LNE = new ListNodeEncode();

        for (int i = 0; i < _ListNode.Count; i++)
        {
            LNE.AddNode(_ListNode[i].Encode());
        }

        LNE.HintPath = _ListPath[0].DecodeForJson();

        string json = JsonUtility.ToJson(LNE);
        string path = "Assets/JsonMap/" + _MapName + ".json";

        File.WriteAllText(path, json);
    }

    [ContextMenu(nameof(LoadMapOnCurrentMapID))]
    public void LoadMapOnCurrentMapID()
    {
        LoadMap(_CurrentMapID);
    }

    [ContextMenu(nameof(ClearListPathIndexInNode))]
    public void ClearListPathIndexInNode()
    {
        for (int i = 0; i < _ListNode.Count; i++)
        {
            _ListNode[i]._ListPathIndex.Clear();
        }
    }

    #endregion


    public void AddNodeToTempPath(Node node)
    {
        if (_CurrentPathID >= _ListPath.Count)
            return;

        for (int i = 0; i < _ListPath[_CurrentPathID]._ListNode.Count; i++)
        {
            if (node == _ListPath[_CurrentPathID]._ListNode[i])
            {
                return;
            }
        }

        if (_ListPath[_CurrentPathID]._CurrentNode != null)
        {
            int dir = node.CheckNode(_ListPath[_CurrentPathID]._CurrentNode);
            if (dir == -1)
                return;

            if (_ListPath[_CurrentPathID]._CurrentNode.SetDir(node) == -1)
                return;
        }
        _ListPath[_CurrentPathID].AddNode(node);

        node.AddPathID(_CurrentPathID, _ListPath[_CurrentPathID]._CurrentNode);
        _ListPath[_CurrentPathID]._CurrentNode = node;
        _ListPath[_CurrentPathID]._EndNode = node;

        _ListEncodePath[_CurrentPathID] = _ListPath[_CurrentPathID].ToString();
    }

    public void RemoveNodeFromTempPath(Node node)
    {
        if (_CurrentPathID >= _ListPath.Count)
            return;

        for (int i = 0; i < _ListPath[_CurrentPathID]._ListNode.Count; i++)
        {
            if (node == _ListPath[_CurrentPathID]._ListNode[i])
            {
                _ListPath[_CurrentPathID]._ListNode.RemoveAt(i);

                _ListEncodePath[_CurrentPathID] = _ListPath[_CurrentPathID].ToString();

                return;
            }
        }
    }

    public void CreateTempPath()
    {
        Node startNode = null;
        Node endNode = null;

        for (int i = 0; i < _ListNode.Count; i++)
        {
            if (_ListNode[i]._TempPath == true)
            {
                _ListTempPathNode.Add(_ListNode[i]);

                if (_ListNode[i]._StartNode == true)
                {
                    startNode = _ListNode[i];
                }

                if (_ListNode[i]._EndNode == true)
                {
                    endNode = _ListNode[i];
                }
            }
        }

        Path path = new Path(startNode, endNode);
        path.SetNodeManager(this);
        path.SetEndNode(endNode);
        path.SetId(_ListPath.Count);

        _ListPath = new List<Path>();
        _ListPath.Add(path);

        for (int i = 0; i < _ListPath.Count; i++)
        {
            if (_ListPath[i].SearchNodeToTempPath() == false)
            {
                _ListPath[i]._Stuck = false;
            }
            else
            {
                _ListConfirmPath.Add(_ListPath[i]);
            }
        }
    }

    public void CreateContinuePath(Path path, Node node)
    {
        Path p = new Path(path, node);
        _ListPath.Add(p);
    }

    public void LoadMap(int MapId)
    {
        _TxtMapID.text = "No. " + MapId.ToString();

        //var listnodeencode = new ListNodeEncode(_JsonMapManager.GetMapDecode(MapId));
        var listnodeencode = new ListNodeEncode(_JsonMapManager.GetAllMapDecode(MapId));
        
        for (int i = 0; i < listnodeencode.TheList.Count; i++)
        {
            _ListNode[listnodeencode.TheList[i].id].SetData(listnodeencode.TheList[i]);

            if (_ListNode[listnodeencode.TheList[i].id]._StartNode == true)
            {
                _StartNode = _ListNode[listnodeencode.TheList[i].id];
            }

            if (_ListNode[listnodeencode.TheList[i].id]._EndNode == true)
            {
                _EndNode = _ListNode[listnodeencode.TheList[i].id];
            }
        }

        _HintPath = new Path(_StartNode, _EndNode);
        var HintCode = listnodeencode.HintPath.Split(':');
        for (int i = 0; i < HintCode.Length; i++)
        {
            int id = int.Parse(HintCode[i]);
            if (id == -1)
                break;
            _ListNode[id]._TempPath = true;
            _HintPath.AddNode(_ListNode[id]);
        }

        _BugObj = GameObject.Instantiate(_PrefabBug, _StartNode.transform.position, Quaternion.identity).GetComponent<BugController>();
        _GateObj = GameObject.Instantiate(_PrefabGate, _EndNode.transform.position, Quaternion.identity);

        _BugObj._NodeManager = this;
        _VirtualJoyStick._Player = _BugObj;
    }

    public void ShowHint()
    {
        _HintObj._HintPath = _HintPath;
        _HintObj.StartMove();
    }

    public void AutoSolve()
    {
        ClosePausePopup();
        _BugObj._HintPath = _HintPath;
        _BugObj.StartMove();
    }

    public void ShowCompletePopup()
    {
        _EndGame = true;

        _PopupComplete.SetActive(true);
        _TheManager._SelectedMapID += 1;
        PlayerPrefs.SetInt("UnlockLevel", _TheManager._SelectedMapID);
        _TheManager._UnlockTotal = _TheManager._SelectedMapID;
    }

    public void ShowPausePopup()
    {
        _PopupPause.SetActive(true);
        _PauseGame = true;
    }

    public void ClosePausePopup()
    {
        _PopupPause.SetActive(false);
        _PauseGame = false;
    }

    public void TriggerShowHint()
    {
        ShowHint();
    }
    public void TriggerAutoSolve()
    {
        AutoSolve();
    }
    public void TriggerReturnHome()
    {
        _Door.ToDefault();
        Invoke(nameof(LoadSceneHome), _Door._MoveTime + 0.1f);
    }
    public void LoadSceneHome()
    {
        SceneManager.LoadScene("Home");
    }

    float _TimeCount = 0;
    float _LenAmount = 3;
    private void Start()
    {
        _ImgProcessBar.fillAmount = 1;
        _TxtLenAmount.text = _LenAmount.ToString();

        try
        {
            _TheManager = GameObject.Find("TheManager").GetComponent<TheManager>();
            _JsonMapManager = _TheManager._JsonManager;
            _JsonMapManager._NodeManager = this;

            LoadMap(_TheManager._SelectedMapID - 1);
        }
        catch(Exception ex)
        {

        }
    }
    private void Update()
    {
        _TimeCount += Time.deltaTime;
        _ImgProcessBar.fillAmount = 1 - (_TimeCount / _TotalTime);
    }
}

[Serializable] 
public class ListNodeEncode
{
    public List<NodeEncode> TheList = new List<NodeEncode>();
    public string HintPath;
    
    public ListNodeEncode()
    {

    }
    public ListNodeEncode(ListNodeEncode LNE)
    {
        TheList = new List<NodeEncode>(LNE.TheList);
        HintPath = LNE.HintPath;
    }
    public ListNodeEncode(List<NodeEncode> List, string hint)
    {
        TheList = new List<NodeEncode>(List);
        HintPath = hint;
    }

    public ListNodeEncode(List<NodeEncode> List)
    {
        TheList = new List<NodeEncode>(List);
    }

    public void AddNode(NodeEncode node)
    {
        TheList.Add(node);
    }
}
