﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorController : MonoBehaviour
{
    public Vector3 _DefaultPosition;
    public Vector3 _FinalPosition;

    public Vector3 _MoveDirection;

    public Vector3 _StartPosition;

    public float _TimeCount = 0;
    public float _MoveTime = 1.0f;

    public bool _ToFinal = false;
    public bool _ToDefault = false;

    public bool _OpenOnStart = true;

    public Image _ImgBg;
    // Start is called before the first frame update
    void Start()
    {
        if (_ImgBg == null)
        {
            _ImgBg = this.GetComponent<Image>();
        }

        if (_OpenOnStart == true)
        {
            ToFinal();
            //FillToFinal();
        }
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (_ToFinal == true)
        {
            //MoveToFinal();
            FillToFinal();
        }
        else if (_ToDefault == true)
        {
            //MoveToDefault();
            FillToDefault();
        }
    }

    [ContextMenu(nameof(SetDefaultPostion))]
    public void SetDefaultPostion()
    {
        _DefaultPosition = this.transform.position;
    }

    [ContextMenu(nameof(SetFinalPosition))]
    public void SetFinalPosition()
    {
        _FinalPosition = this.transform.position;
    }

    [ContextMenu(nameof(ResetToDefault))]
    public void ResetToDefault()
    {
        this.transform.position = _DefaultPosition;
    }

    public void ToFinal()
    {
        _MoveDirection = _FinalPosition - this.transform.position;
        _StartPosition = this.transform.position;
        _ToFinal = true;

        _TimeCount = 0;

        _ImgBg.fillAmount = 1;
    }

    void MoveToFinal()
    {
        //Vector3 _MoveDirection = _FinalPosition - this.transform.position;
        _TimeCount += Time.fixedDeltaTime;
        if (_TimeCount >= _MoveTime)
        {
            this.transform.position = _FinalPosition;
            _ToFinal = false;
            return;
        }

        float alpha = (180.0f / _MoveTime) * _TimeCount + 180.0f;
        float rate = (Mathf.Cos(alpha * Mathf.Deg2Rad) + 1.0f) / 2.0f;

        this.transform.position = _StartPosition + _MoveDirection * rate;
    }

    void FillToFinal()
    {
        _TimeCount += Time.fixedDeltaTime;
        if (_TimeCount >= _MoveTime)
        {
            //this.transform.position = _FinalPosition;
            _ImgBg.fillAmount = 0;
            _ToFinal = false;
            _ImgBg.raycastTarget = false;
            return;
        }

        float alpha = (180.0f / _MoveTime) * _TimeCount + 180.0f;
        float rate = (Mathf.Cos(alpha * Mathf.Deg2Rad) + 1.0f) / 2.0f;

        _ImgBg.fillAmount = 1 - rate;
    }

    public void ToDefault()
    {
        _MoveDirection = _DefaultPosition - this.transform.position;
        _StartPosition = this.transform.position;
        _ToDefault = true;

        _TimeCount = 0;

        _ImgBg.fillAmount = 0;

        _ImgBg.raycastTarget = true;
    }

    void MoveToDefault()
    {
        //Vector3 _MoveDirection = _FinalPosition - this.transform.position;
        _TimeCount += Time.fixedDeltaTime;
        if (_TimeCount >= _MoveTime)
        {
            this.transform.position = _DefaultPosition;
            _ToDefault = false;
            return;
        }

        float alpha = (180.0f / _MoveTime) * _TimeCount + 180.0f;
        float rate = (Mathf.Cos(alpha * Mathf.Deg2Rad) + 1.0f) / 2.0f;

        this.transform.position = _StartPosition + _MoveDirection * rate;
    }

    void FillToDefault()
    {
        _TimeCount += Time.fixedDeltaTime;
        if (_TimeCount >= _MoveTime)
        {
            //this.transform.position = _FinalPosition;
            _ImgBg.fillAmount = 1;
            _ToFinal = false;
            return;
        }

        float alpha = (180.0f / _MoveTime) * _TimeCount + 180.0f;
        float rate = (Mathf.Cos(alpha * Mathf.Deg2Rad) + 1.0f) / 2.0f;

        _ImgBg.fillAmount = rate;
    }
}
