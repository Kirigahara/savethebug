﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapItem : MonoBehaviour
{
    public MapSpawn _MapSpawn;

    public Image _ImgTop;
    public Image _ImgRight;
    public Image _ImgLock;

    public Text _TxtID;

    public int _MapID = 0;
    public int _NextMapID = 0;

    public bool _BeLock;

    public void ShowRight()
    {
        _ImgRight.gameObject.SetActive(true);
    }

    public void ShowTop()
    {
        _ImgTop.gameObject.SetActive(true);
    }

    public void HideRight()
    {
        _ImgRight.gameObject.SetActive(false);
    }

    public void HideTop()
    {
        _ImgTop.gameObject.SetActive(false);
    }

    public void TriggerBtn()
    {
        if (_MapID > 4)
            return;

        _MapSpawn._TheManager._SelectedMapID = _MapID;
        _MapSpawn.GoToGame();
    }

    public void Lock()
    {
        _ImgLock.enabled = true;
        _BeLock = true;
    }

    public void Unlock()
    {
        _ImgLock.enabled = false;
        _BeLock = false;
    }
}
