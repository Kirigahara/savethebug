﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugController : MonoBehaviour
{
    public NodeManager _NodeManager;

    public Path _HintPath;

    public TrailRenderer _Trail;
    public ParticleSystem _TrailParticle;
    public Rigidbody2D _Rigibody;

    public int _CurrentNodeId = 0;
    public int _NextNodeID = 0;

    public bool _OnAutoMove = false;
    public bool _TouchControl = false;
    public bool _OnFinish = false;

    public float _MoveTime = 1.0f;
    public float _TimeCount = 0;
    public float _Speed = 1.0f;
    public float _Rate;

    public Vector3 _DefaultPosition;
    public Vector3 _MoveVec;
    public Vector3 _MoveVecAuto;
    public Vector3 _NextPosition;
    public Vector3 _StartPosition;
    public Vector3 _SpawnPosition;

    public Node _LastTempNode = null;
    public Node _CurrentNode = null;

    // Start is called before the first frame update
    void Start()
    {
        _SpawnPosition = this.transform.position;
    }

    // Update is called once per frame

    private void FixedUpdate() 
    {
        if(_NodeManager._PauseGame == true)
        {
            _Rigibody.velocity = Vector3.zero;
            return;
        }

        if(_OnAutoMove == true)
        {
            if(_OnFinish == true)
            {
                _OnAutoMove = false;
                _NodeManager.ShowCompletePopup();
                return;
            }
            else
            {
                MoveToNext();
            }
        }
        else
        {
            if(_TouchControl == true)
            {
                this.transform.rotation = LookToTarget2D(_MoveVec);
                _Rigibody.velocity = _Speed * _MoveVec;
            }
        }
    }

    public void TouchController(Vector3 MoveVec, bool DisableAuto = true)
    {
        _MoveVec = MoveVec.normalized;
        _TouchControl = true;
        //if (DisableAuto == true)
        //    _OnAutoMove = false;
    }

    public void EndTouchController()
    {
        _MoveVec = Vector3.zero;
        _TouchControl = false;
    }

    public void StartMove()
    {
        if (_OnAutoMove == false)
        {
            this.transform.position = _SpawnPosition;

            _OnAutoMove = true;
            //_OnFinish = false;
            _CurrentNodeId = 0;
            this.transform.position = _HintPath._ListNode[_CurrentNodeId].gameObject.transform.position;

            _StartPosition = this.transform.position;
            _NextNodeID = _CurrentNodeId + 1;

            _NextPosition = NextNodePosition();
            _MoveVecAuto = _NextPosition - this.transform.position;
        }
        else
        {
            _OnAutoMove = false;
        }
    }

    public void MoveToNext()
    {
        _Rate = (1.0f / _MoveTime) * _TimeCount;
        //_Rate = Mathf.Min(_Rate, 1.0f);

        if (_TimeCount >= _MoveTime)
        {
            //this.transform.position = _NextPosition;
            _StartPosition = this.transform.position;
            _CurrentNodeId = _NextNodeID;
            _NextNodeID += 1;
            _NextPosition = NextNodePosition();
            _MoveVecAuto = _NextPosition - this.transform.position;
            _TimeCount = 0;

            return;
        }

        this.transform.position = _StartPosition + _Rate * _MoveVecAuto;

        _TimeCount += Time.fixedDeltaTime;

        this.transform.rotation = LookToTarget2D(_MoveVecAuto, 0.9f);

        //Vector3 ptd = _NextPosition - this.transform.position;
        //if (ptd.sqrMagnitude < 0.05f)
        //{
        //    _CurrentNodeId = _NextNodeID;
        //    _NextNodeID += 1;
        //    _NextPosition = NextNodePosition();
        //    _MoveVec = _NextPosition - this.transform.position;
        //    _TimeCount = 0;

        //    if (_OnFinish == true)
        //        return;
        //}
        //else
        //{
        //    this.transform.rotation = LookToTarget2D(ptd, 0.9f);
        //    this.transform.position += this.transform.TransformDirection(Vector2.right) * _Speed * Time.fixedDeltaTime;
        //}
    }

    public Vector3 NextNodePosition()
    {
        if (_HintPath._ListNode[_CurrentNodeId]._EndNode == true)
        {
            //_OnFinish = true;
            return Vector3.zero;
        }

        if (_NextNodeID < _HintPath._ListNode.Count)
        {
            return _HintPath._ListNode[_NextNodeID].transform.position;
        }
        return Vector3.zero;
    }

    public Quaternion LookToTarget2D(Vector3 dir, float percent = 1.0f)
    {
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg * percent;
        return Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public void Stop()
    {
        _Rigibody.velocity = Vector3.zero;
        _TouchControl = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch(collision.gameObject.tag)
        {
            case "Gate":
                {
                    _OnFinish = true;
                    _OnAutoMove = false;
                    _TouchControl = false;

                    _NodeManager.ShowCompletePopup();
                    break;
                }
            case "Node":
                {
                    Node node = collision.gameObject.GetComponent<Node>();
                    if(node._TempPath==true)
                    {
                        _LastTempNode = node;
                    }
                    _CurrentNode = node;

                    break;
                }
            case "Bug":
                {
                    break;
                }
        }
    }
}
