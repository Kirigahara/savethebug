﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JsonMapManager : MonoBehaviour
{
    public NodeManager _NodeManager;

    public List<TextAsset> _ListTextAsset = new List<TextAsset>();

    public string GetMap(int id)
    {
        return _ListTextAsset[id].text;
    }

    public string GetMap(string name)
    {
        for (int i = 0; i < _ListTextAsset.Count; i++)
        {
            if (_ListTextAsset[i].name == name)
            {
                return _ListTextAsset[i].text;
            }
        }

        return "";
    }

    public List<NodeEncode> GetMapDecode(int id)
    {
        string text = GetMap(id);
        ListNodeEncode jn = JsonUtility.FromJson<ListNodeEncode>(text);

        return jn.TheList;

    }

    public List<NodeEncode> GetMapDecode(string name)
    {
        string text = GetMap(name);
        ListNodeEncode jn = JsonUtility.FromJson<ListNodeEncode>(text);

        return jn.TheList;

    }

    public ListNodeEncode GetAllMapDecode(int id)
    {
        string text = GetMap(id);
        ListNodeEncode jn = JsonUtility.FromJson<ListNodeEncode>(text);

        return jn;

    }
}
