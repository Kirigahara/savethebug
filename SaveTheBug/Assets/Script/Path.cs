﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using System;

[Serializable]
public class Path
{
    public NodeManager _NodeManager;
    public Navigator _Navigator;
    
    public int _Id = 0;
    public List<Node> _ListNode = new List<Node>();

    public Node _StartNode;
    public Node _EndNode;

    public Node _CurrentNode;

    public bool _Stuck = false;

    string _Encode = "";
    public Path()
    {

    }
    public Path(Node StartNode, Node EndNode)
    {
        _ListNode.Add(StartNode);
        //StartNode.AddPathID(_Id, null);

        SetStartNode(StartNode);

        _StartNode = StartNode;
        _EndNode = EndNode;

        _CurrentNode = StartNode;
    }
    public Path(Path path)
    {
        ContinuePath(path);

        _CurrentNode = path._CurrentNode;
    }
    public Path(Path path, Node node)
    {
        ContinuePath(path);
        _ListNode.Add(node);

        _CurrentNode = node;
    }

    public void ContinuePath(Path path)
    {
        _ListNode = new List<Node>(path._ListNode);
        
        _StartNode = path._StartNode;
        _EndNode = path._EndNode;
    }

    public bool SearchNodeToTempPath()
    {
        return _CurrentNode.SearchNeighborForTempPath(this, _CurrentNode);
        //_CurrentNode.SearchNeighbor(this);

    }

    public bool SearchNode()
    {
        return _CurrentNode.SearchNeighbor(this, _CurrentNode);
        //_CurrentNode.SearchNeighbor(this);

    }

    public bool AddNode(Node node)
    {
        if (node.CheckPathID(_Id) == true)
        {
            if (_ListNode.Count == 0)
            {
                _StartNode = node;
            }

            _CurrentNode = node;
            _ListNode.Add(node);

            return true;
        }
        return false;
    }

    public bool CheckPrevious(Node node)
    {
        if (node == null)
            return false;

        if (_ListNode.Count > 1)
        {
            if (_ListNode[_ListNode.Count - 2] == node)
                return true;
        }

        return false;
    }

    public Path Duplicate()
    {
        Path p = new Path(this);
        p._StartNode = _StartNode;
        p._CurrentNode = _CurrentNode;
        p._EndNode = _EndNode;

        return p;
    }

    public void NormalizeIDList()
    {
        for (int i = 0; i < _ListNode.Count; i++)
        {
            _ListNode[i].AddPathID(_Id);
        }
    }

    public override string ToString()
    {
        _Encode = "";
        //return base.ToString();
        for (int i = 0; i < _ListNode.Count; i++)
        {
            _Encode += _ListNode[i].ToString() + "|";
        }

        return _Encode;
    }

    public void Decode()
    {
        var nodeEncode = _Encode.Split('|');
        List<NodeAndDir> nodeAndDir = new List<NodeAndDir>();

        for (int i = 0; i < nodeEncode.Length; i++)
        {
            var code = nodeEncode[i].Split(':');
            if (code.Length == 0)
                break;
            nodeAndDir.Add(new NodeAndDir(int.Parse(code[0]), int.Parse(code[1])));
            _NodeManager._ListNode[int.Parse(code[0])].LockAll();

            if (int.Parse(code[1]) == -1)
                break;
        }


        //for (int i = 0; i < nodeAndDir.Count; i++)
        //{
        //    if (nodeAndDir[i]._Dir != -1)
        //    {
        //        _NodeManager._ListNode[nodeAndDir[i]._Id]._NextNode = _ListNode[nodeAndDir[i + 1]._Id];
        //        _NodeManager._ListNode[nodeAndDir[i]._Id].UnlockOnNextNode();
        //    }
        //}

        for (int i = 0; i < _ListNode.Count; i++)
        {
            if (i + 1 >= _ListNode.Count)
                break;

            _ListNode[i]._NextNode = _ListNode[i + 1];
            _ListNode[i].UnlockOnNextNode();
        }
    }

    public void HideSign()
    {
        for(int i=0;i<_ListNode.Count;i++)
        {
            _ListNode[i].HideSign();
        }
    }

    public void BlockSign()
    {
        for (int i = 0; i < _ListNode.Count; i++)
        {
            _ListNode[i].BlockSign();
        }
    }

    public void SafeSign()
    {
        for (int i = 0; i < _ListNode.Count; i++)
        {
            _ListNode[i].SafeSign();
        }
    }

    public string DecodeForJson()
    {
        string code = "";

        for (int i = 0; i < _ListNode.Count; i++)
        {
            code += _ListNode[i]._Id + ":";
        }

        code += "-1";

        return code;
    }

    public void ClearPath()
    {
        _ListNode.Clear();
    }

    public void SetId(int id)
    {
        _Id = id;
    }

    public void SetStartNode(Node startNode)
    {
        _StartNode = startNode;
    }

    public void SetEndNode(Node endNode)
    {
        _EndNode = endNode;
    }
    public void SetNodeManager(NodeManager nodeManager)
    {
        _NodeManager = nodeManager;
    }
}

public class NodeAndDir
{
    public int _Id;
    public int _Dir;

    public NodeAndDir(int id, int dir)
    {
        _Id = id;
        _Dir = dir;
    }
}
