﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Serialization;

public class Node : MonoBehaviour
{
    public PathManager _PathManager;
    public NodeManager _NodeManager;

    public int _Id;

    public Node _Top;
    public Node _Right;
    public Node _Down;
    public Node _Left;

    public bool _LockTop = false;
    public bool _LockLeft = false;
    public bool _LockDown = false;
    public bool _LockRight = false;

    public bool _StartNode = false;
    public bool _EndNode = false;

    public List<PathDirection> _ListPathIndex = new List<PathDirection>();

    public float _G;
    public float _H;
    public float _F;

    public bool _TempPath = false;

    public Node _NextNode;
    public Node _PreviousNode;

    int _Dir = -1;
    public bool _ForceUnlockTop = false;
    public bool _ForceUnlockRight = false;
    public bool _ForceUnlockDown = false;
    public bool _ForceUnlockLeft = false;

    [Header("Visualize")]
    public SpriteRenderer _SpriteMain;
    public SpriteRenderer _SpriteTop;
    public SpriteRenderer _SpriteLeft;
    public SpriteRenderer _SpriteDown;
    public SpriteRenderer _SpriteRight;

    public SpriteRenderer _SpriteStart;
    public SpriteRenderer _SpriteEnd;

    public SpriteRenderer _SpriteSign;

    public string _BlockColorCode = "785019";

    [Header("Physics")]
    public Collider2D _ColliderMain;
    public Collider2D _ColliderTop;
    public Collider2D _ColliderRight;
    public Collider2D _ColliderDown;
    public Collider2D _ColliderLeft;

    [ContextMenu(nameof(ClearStartEnd))]
    public void ClearStartEnd()
    {
        _SpriteStart.enabled = false;
        _SpriteEnd.enabled = false;
    }
    [ContextMenu(nameof(SetStart))]
    public void SetStart()
    {
        _StartNode = true;
        _SpriteStart.enabled = true;
    }
    [ContextMenu(nameof(SetEnd))]
    public void SetEnd()
    {
        _EndNode = true;
        _SpriteEnd.enabled = true;
    }
    public void SelectionTempPath()
    {
        _TempPath = true;
        _SpriteSign.enabled = true;
        _SpriteSign.color = Color.green;

        _NodeManager.AddNodeToTempPath(this);

    }
    public void UnSelectionTempPath()
    {
        _TempPath = false;
        _SpriteSign.enabled = false;

        _NodeManager.RemoveNodeFromTempPath(this);
    }

    public void ShowStart()
    {
        _SpriteStart.enabled = true;
    }
    public void ShowEnd()
    {
        _SpriteEnd.enabled = true;
    }

    public bool CheckNeighbor(Node node)
    {
        if (node == _Top && _LockTop == false)
            return true;
        if (node == _Left && _LockLeft == false)
            return true;
        if (node == _Down && _LockDown == false)
            return true;
        if (node == _Right && _LockRight == false)
            return true;

        return false;
    }

    public bool AddPathID(int id, Node node)
    {
        for (int i = 0; i < _ListPathIndex.Count; i++)
        {
            if (_ListPathIndex[i]._PathId == id)
                return false;
        }

        _ListPathIndex.Add(new PathDirection(id, CheckNode(node)));

        return true;
    }

    public bool AddPathID(int id)
    {
        for (int i = 0; i < _ListPathIndex.Count; i++)
        {
            if (_ListPathIndex[i]._PathId == id)
                return false;
        }
        return true;
    }

    public int CheckNode(Node node)
    {
        if (node == null)
            return -1;
        if (node == _Top)
            return 0;
        if (node == _Right)
            return 1;
        if (node == _Down)
            return 2;
        if (node == _Left)
            return 3;

        return -1;
    }

    public bool CheckPathID(int id)
    {
        for (int i = 0; i < _ListPathIndex.Count; i++)
        {
            if (_ListPathIndex[i]._PathId == id)
                return false;
        }
        return true;
    }

    public bool SearchNeighborForTempPath(Path path, Node previous)
    {
        bool ConfirmToPath = false;

        if (_Top != previous)
        {
            if (_Top._TempPath == true)
            {
                if (_Top._EndNode == false)
                {
                    return true;
                }

                if (ConfirmToPath == false)
                {
                    ConfirmToPath = true;
                    if (path.AddNode(_Top) == false)
                    {
                        return false;
                    }
                    if (_Top.AddPathID(path._Id, previous) == false)
                    {
                        return false;
                    }
                    return _Top.SearchNeighborForTempPath(path, _Top);
                }
                else
                {
                    path._NodeManager.CreateContinuePath(path, _Top);
                }
            }
        }
        if (_Left != previous)
        {
            if (_Left._TempPath == true)
            {
                if (_Left._EndNode == false)
                {
                    return true;
                }

                if (ConfirmToPath == false)
                {
                    ConfirmToPath = true;
                    if (path.AddNode(_Left) == false)
                    {
                        return false;
                    }
                    if (_Left.AddPathID(path._Id, previous) == false)
                    {
                        return false;
                    }
                    return _Left.SearchNeighborForTempPath(path, _Left);
                }
                else
                {
                    path._NodeManager.CreateContinuePath(path, _Left);
                }
            }
        }
        if (_Down != previous)
        {
            if (_Down._TempPath == true)
            {
                if (_Down._EndNode == false)
                {
                    return true;
                }

                if (ConfirmToPath == false)
                {
                    ConfirmToPath = true;
                    if (path.AddNode(_Down) == false)
                    {
                        return false;
                    }
                    if (_Down.AddPathID(path._Id, previous) == false)
                    {
                        return false;
                    }
                    return _Down.SearchNeighborForTempPath(path, _Down);
                }
                else
                {
                    path._NodeManager.CreateContinuePath(path, _Down);
                }
            }
        }
        if (_Right != previous)
        {
            if (_Right._TempPath == true)
            {
                if (_Right._EndNode == false)
                {
                    return true;
                }

                if (ConfirmToPath == false)
                {
                    ConfirmToPath = true;
                    if (path.AddNode(_Right) == false)
                    {
                        return false;
                    }
                    if (_Top.AddPathID(path._Id, previous) == false)
                    {
                        return false;
                    }
                    return _Right.SearchNeighborForTempPath(path, _Right);
                }
                else
                {
                    path._NodeManager.CreateContinuePath(path, _Right);
                }
            }
        }

        return false;
    }

    public bool SearchNeighbor(Path path, Node previous)
    {
        bool ConfirmToPath = false;
        bool RightPath = false;
        Path ClonePath = path.Duplicate();

        if (ClonePath.CheckPrevious(_Top) == false && _Top != null)
        {
            if (_Top._EndNode == true)
            {
                RightPath = true;
                return true;
            }
            if (_LockTop == false)
            {
                if (ConfirmToPath == false)
                {
                    ConfirmToPath = true;
                    if (path.AddNode(_Top) == false)
                    {
                        return false;
                    }
                    if (_Top.AddPathID(path._Id, previous) == false)
                    {
                        return false;
                    }
                    //return _Top.SearchNeighbor(path, _Top);
                    if (_Top.SearchNeighbor(path, _Top) == false)
                    {
                        _SpriteSign.color = Color.red;
                    }
                    else
                    {
                        RightPath = true;
                    }
                }
                else
                {
                    path._Navigator.CreateContinuePath(ClonePath, _Top, this);
                }
            }
        }
        if (ClonePath.CheckPrevious(_Left) == false && _Left != null)
        {
            if (_Left._EndNode == true)
            {
                RightPath = true;
                return true;
            }
            if (_LockLeft == false)
            {
                if (ConfirmToPath == false)
                {
                    ConfirmToPath = true;
                    if (path.AddNode(_Left) == false)
                    {
                        return false;
                    }
                    if (_Left.AddPathID(path._Id, previous) == false)
                    {
                        return false;
                    }
                    //return _Left.SearchNeighbor(path, _Left);
                    if (_Left.SearchNeighbor(path, _Left) == false)
                    {
                        _SpriteSign.color = Color.red;
                    }
                    else
                    {
                        RightPath = true;
                    }
                }
                else
                {
                    path._Navigator.CreateContinuePath(ClonePath, _Left, this);
                }
            }
        }
        if (ClonePath.CheckPrevious(_Down) == false && _Down != null)
        {
            if (_Down._EndNode == true)
            {
                RightPath = true;
                return true;
            }
            if (_LockDown == false)
            {
                if (ConfirmToPath == false)
                {
                    ConfirmToPath = true;
                    if (path.AddNode(_Down) == false)
                    {
                        return false;
                    }
                    if (_Down.AddPathID(path._Id, previous) == false)
                    {
                        return false;
                    }
                    //return _Down.SearchNeighbor(path, _Down);
                    if (_Down.SearchNeighbor(path, _Down) == false)
                    {
                        _SpriteSign.color = Color.red;
                    }
                    else
                    {
                        RightPath = true;
                    }
                }
                else
                {
                    path._Navigator.CreateContinuePath(ClonePath, _Down, this);
                }
            }
        }
        if (ClonePath.CheckPrevious(_Right) == false && _Right != null)
        {
            if (_Right._EndNode == true)
            {
                RightPath = true;
                return true;
            }
            if (_LockRight == false)
            {
                if (ConfirmToPath == false)
                {
                    ConfirmToPath = true;
                    if (path.AddNode(_Right) == false)
                    {
                        return false;
                    }
                    if (_Right.AddPathID(path._Id, previous) == false)
                    {
                        return false;
                    }
                    //return _Right.SearchNeighbor(path, _Right);
                    if (_Right.SearchNeighbor(path, _Right) == false)
                    {
                        _SpriteSign.color = Color.red;
                    }
                    else
                    {
                        RightPath = true;
                    }
                }
                else
                {
                    path._Navigator.CreateContinuePath(ClonePath, _Right, this);
                }
            }
        }

        if (RightPath == true)
            return true;
        else
            return false;
    }

    public void GetNextNode()
    {

    }
    public void ClearNode()
    {
        _Top = null;
        _Right = null;
        _Down = null;
        _Left = null;
    }
    public void ResetNode()
    {
        Unlock();

        _TempPath = false;
        _SpriteSign.enabled = false;

        _ForceUnlockTop = false;
        _ForceUnlockRight = false;
        _ForceUnlockLeft = false;
        _ForceUnlockDown = false;

        _ListPathIndex.Clear();
    }

    public void ResetTemp()
    {
        _TempPath = false;
        _SpriteSign.enabled = false;
    }

    public void LockAll()
    {
        if (_ForceUnlockTop == false)
        {
            LockTop();
        }
        if (_ForceUnlockLeft == false)
        {
            LockLeft();
        }
        if (_ForceUnlockDown == false)
        {
            LockDown();
        }
        if (_ForceUnlockRight == false)
        {
            LockRight();
        }
    }

    public void UnlockOnDir(int dir)
    {
        switch (dir)
        {
            case 0:
                {
                    UnlockTop();
                    break;
                }
            case 1:
                {
                    UnlockRight();
                    break;
                }
            case 2:
                {
                    UnlockDown();
                    break;
                }
            case 3:
                {
                    UnlockLeft();
                    break;
                }
            default:
                {

                    break;
                }
        }
    }
    public void UnlockOnNextNode()
    {
        if (_NextNode == null)
            return;

        if(_NextNode == _Top)
        {
            _ForceUnlockTop = true;
            UnlockTop();
            
            _NextNode.UnlockDown();
            _NextNode._ForceUnlockDown = true;
        }
        if (_NextNode == _Right)
        {
            _ForceUnlockRight = true;
            UnlockRight();

            _NextNode.UnlockLeft();
            _NextNode._ForceUnlockLeft = true;
        }
        if (_NextNode == _Down)
        {
            _ForceUnlockDown = true;
            UnlockDown();
            
            _NextNode.UnlockTop();
            _NextNode._ForceUnlockTop = true;
        }
        if (_NextNode == _Left)
        {
            _ForceUnlockLeft = true;
            UnlockLeft();
            
            _NextNode.UnlockRight();
            _NextNode._ForceUnlockRight = true;
        }

    }

    public void RandomLock()
    {
        for (int i = 0; i < 4; i++)
        {
            int decide = UnityEngine.Random.Range(0, 2);
            switch (decide)
            {
                case 0:
                    {
                        int lockDir = UnityEngine.Random.Range(0, 5);

                        switch (lockDir)
                        {
                            case 0:
                                {
                                    if (_ForceUnlockTop == false)
                                    {
                                        LockTop();
                                    }
                                    break;
                                }
                            case 1:
                                {
                                    if (_ForceUnlockRight == false)
                                    {
                                        LockRight();
                                    }
                                    break;
                                }
                            case 2:
                                {
                                    if (_ForceUnlockDown == false)
                                    {
                                        LockDown();
                                    }
                                    break;
                                }
                            case 3:
                                {
                                    if (_ForceUnlockLeft == false)
                                    {
                                        LockLeft();
                                    }
                                    break;
                                }
                        }

                        break;
                    }
                case 1:
                    {
                        int unlockDir = UnityEngine.Random.Range(0, 5);

                        switch (unlockDir)
                        {
                            case 0:
                                {
                                    UnlockTop();
                                    break;
                                }
                            case 1:
                                {
                                    UnlockRight();
                                    break;
                                }
                            case 2:
                                {
                                    UnlockDown();
                                    break;
                                }
                            case 3:
                                {
                                    UnlockLeft();
                                    break;
                                }
                        }


                        break;
                    }
            }
        }
    }

    public int SetDir(Node node)
    {
        _Dir = CheckNode(node);
        return _Dir;
    }

    public override string ToString()
    {
        return _Id.ToString() + ":" + _Dir.ToString();
    }

    public void HideMain()
    {
        _SpriteMain.enabled = false;
    }
    public void ShowMain()
    {
        _SpriteMain.enabled = true;
    }

    public void HideSign()
    {
        _SpriteSign.enabled = false;
        _SpriteSign.color = Color.white;
    }

    public void BlockSign()
    {
        _SpriteSign.enabled = true;
        _SpriteSign.color = Color.red;
    }

    public void SafeSign()
    {
        _SpriteSign.enabled = true;
        _SpriteSign.color = Color.green;
    }

    public NodeEncode Encode()
    {
        int status = 0;
        if (_StartNode == true)
            status = 1;
        else if (_EndNode == true)
            status = 2;

        return new NodeEncode(_Id, status, _LockTop, _LockRight, _LockDown, _LockLeft);
    }

    public void SetData(NodeEncode code)
    {
        _ColliderMain.isTrigger = true;

        switch(code.status)
        {
            case 0:
                {
                    _StartNode = false;
                    _EndNode = false;

                    _SpriteStart.enabled = false;
                    _SpriteEnd.enabled = false;

                    break;
                }
            case 1:
                {
                    _StartNode = true;
                    //_SpriteStart.enabled = true;

                    break;
                }
            case 2:
                {
                    _EndNode = true;
                    //_SpriteEnd.enabled = true;

                    break;
                }
        }

        if (code.locktop == 1)
            LockTop();
        else
            UnlockTop();

        if (code.lockright == 1)
            LockRight();
        else
            UnlockRight();

        if (code.lockdown == 1)
            LockDown();
        else
            UnlockDown();

        if (code.lockleft == 1)
            LockLeft();
        else
            UnlockLeft();
    }

    [ContextMenu(nameof(LockTop))]
    public void LockTop()
    {
        _LockTop = true;
        _SpriteTop.enabled = true;
        _ColliderTop.enabled = true;

        if (_Top != null)
        {
            _Top._LockDown = true;
            _Top._SpriteDown.enabled = true;
            _Top._ColliderDown.enabled = true;
        }
    }
    [ContextMenu(nameof(LockRight))]
    public void LockRight()
    {
        _LockRight = true;
        _SpriteRight.enabled = true;
        _ColliderRight.enabled = true;

        if (_Right != null)
        {
            _Right._LockLeft = true;
            _Right._SpriteLeft.enabled = true;
            _Right._ColliderLeft.enabled = true;
        }
    }
    [ContextMenu(nameof(LockDown))]
    public void LockDown()
    {
        _LockDown = true;
        _SpriteDown.enabled = true;
        _ColliderDown.enabled = true;

        if (_Down != null)
        {
            _Down._LockTop = true;
            _Down._SpriteTop.enabled = true;
            _Down._ColliderTop.enabled = true;
        }
    }
    [ContextMenu(nameof(LockLeft))]
    public void LockLeft()
    {
        _LockLeft = true;
        _SpriteLeft.enabled = true;
        _ColliderLeft.enabled = true;

        if (_Left != null)
        {
            _Left._LockRight = true;
            _Left._SpriteRight.enabled = true;
            _Left._ColliderRight.enabled = true;
        }
    }
    [ContextMenu(nameof(Unlock))]
    public void Unlock()
    {
        _LockTop = false;
        _LockRight = false;
        _LockDown = false;
        _LockLeft = false;

        _SpriteTop.enabled = false;
        _SpriteRight.enabled = false;
        _SpriteDown.enabled = false;
        _SpriteLeft.enabled = false;

        _ColliderTop.enabled = false;
        _ColliderRight.enabled = false;
        _ColliderDown.enabled = false;
        _ColliderLeft.enabled = false;
    }
    [ContextMenu(nameof(UnlockTop))]
    public void UnlockTop()
    {
        if (_Top != null)
        {
            _LockTop = false;
            _SpriteTop.enabled = false;
            _ColliderTop.enabled = false;

            _Top._LockDown = false;
            _Top._SpriteDown.enabled = false;
            _Top._ColliderDown.enabled = false;
        }
    }
    [ContextMenu(nameof(UnlockRight))]
    public void UnlockRight()
    {
        if (_Right != null)
        {
            _LockRight = false;
            _SpriteRight.enabled = false;
            _ColliderRight.enabled = false;

            _Right._LockLeft = false;
            _Right._SpriteLeft.enabled = false;
            _Right._ColliderLeft.enabled = false;
        }
    }
    [ContextMenu(nameof(UnlockDown))]
    public void UnlockDown()
    {
        if (_Down != null)
        {
            _LockDown = false;
            _SpriteDown.enabled = false;
            _ColliderDown.enabled = false;

            _Down._LockTop = false;
            _Down._SpriteTop.enabled = false;
            _Down._ColliderTop.enabled = false;
        }
    }
    [ContextMenu(nameof(UnlockLeft))]
    public void UnlockLeft()
    {
        if (_Left != null)
        {
            _LockLeft = false;
            _SpriteLeft.enabled = false;
            _ColliderLeft.enabled = false;

            _Left._LockRight = false;
            _Left._SpriteRight.enabled = false;
            _Left._ColliderRight.enabled = false;
        }
    }
}

[Serializable]
public class PathDirection
{
    public int _PathId = -1;
    public int _DirId = -1;

    public PathDirection(int id, int dir)
    {
        _PathId = id;
        _DirId = dir;
    }
}

[Serializable]
public class NodeEncode
{
    //1 start node
    //2 end node
    //0 normal node
    public int id = 0;
    public int status = 0;
    public int locktop = 0;
    public int lockright = 0;
    public int lockdown = 0;
    public int lockleft = 0;

    public NodeEncode(int Id, int Status, bool LockTop, bool LockRight, bool LockDown, bool LockLeft)
    {
        status = Status;

        id = Id;
        locktop = ConverBoolToInt(LockTop);
        lockright = ConverBoolToInt(LockRight);
        lockdown = ConverBoolToInt(LockDown);
        lockleft = ConverBoolToInt(LockLeft);

    }

    private int ConverBoolToInt(bool condition)
    {
        if (condition == true)
            return 1;

        return 0;
    }
}