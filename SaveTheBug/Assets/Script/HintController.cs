﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HintController : MonoBehaviour
{
    public Path _HintPath;

    public ParticleSystem _MainParticle;
    public ParticleSystem _TrailParticle;
    public TrailRenderer _Trail;
    
    public int _CurrentNodeId = 0;
    public int _NextNodeID = 0;

    public bool _OnMoveHint = false;
    public bool _OnFinishHint = false;

    public float _MoveTime = 1.0f;    
    public float _TimeCount = 0;
    public float rate = 0;

    public Vector3 _DefaultPosition;
    public Vector3 _MoveVec;
    public Vector3 _MoveDirection;
    public Vector3 _NextPosition;
    public Vector3 _StartPosition;

    // Start is called before the first frame update
    void Start()
    {
        SetDefaultPosition();
    }

    // Update is called once per frame
    void Update()
    {
        if (_OnMoveHint == true)
        {
            if (_OnFinishHint == true)
            {
                _OnMoveHint = false;
                Hide();
                return;
            }
            else
            {
                MoveToNext();
            }

        }
    }

    public void StartMove()
    {
        if (_OnMoveHint == false)
        {
            Show();

            _OnMoveHint = true;
            _OnFinishHint = false;
            _CurrentNodeId = 0;
            this.transform.position = _HintPath._ListNode[_CurrentNodeId].gameObject.transform.position;

            _StartPosition = this.transform.position;
            _NextNodeID = _CurrentNodeId + 1;

            _NextPosition = NextNodePosition();
            _MoveVec = _NextPosition - this.transform.position;
        }
    }

    public void MoveToNext()
    {
        rate = (1.0f / _MoveTime) * _TimeCount;
        //rate = Mathf.Min(rate, 1.0f);
        if (_TimeCount >= _MoveTime)
        {
            this.transform.position = _NextPosition; 
            _StartPosition = this.transform.position;
            _CurrentNodeId = _NextNodeID;
            _NextNodeID += 1;
            _NextPosition = NextNodePosition();
            _MoveVec = _NextPosition - this.transform.position;
            _TimeCount = 0;

            return;
        }

        this.transform.position = _StartPosition + rate * _MoveVec;

        _TimeCount += Time.deltaTime;

        //_MoveDirection = (_NextPosition - this.transform.position);
        //if (_MoveDirection.sqrMagnitude < 0.05f)
        //{
        //    this.transform.position = _NextPosition;
        //    _CurrentNodeId = _NextNodeID;
        //    _NextNodeID += 1;
        //    _NextPosition = NextNodePosition();
        //    _MoveDirection = (_NextPosition - this.transform.position);
        //    _MoveVec = _MoveDirection.normalized;
        //    _TimeCount = 0;
        //}
        //else
        //{
        //    this.transform.position += _MoveVec * 2 * Time.deltaTime;
        //}
        //_TimeCount += Time.deltaTime;
    }

    public Vector3 NextNodePosition()
    {
        if(_HintPath._ListNode[_CurrentNodeId]._EndNode == true)
        {
            _OnFinishHint = true;
            return Vector3.zero;
        }

        if (_NextNodeID < _HintPath._ListNode.Count)
        {
            return _HintPath._ListNode[_NextNodeID].transform.position;
        }
        return Vector3.zero;
    }

    public void Show()
    {
        CancelInvoke(nameof(CompleteHide));

        _MainParticle.gameObject.SetActive(true);
        _TrailParticle.gameObject.SetActive(true);

        _MainParticle.Play();
        _TrailParticle.Play();
        _Trail.enabled = true;

    }

    public void Hide()
    {
        _MainParticle.Stop();
        _TrailParticle.Stop();
        _Trail.Clear();

        Invoke(nameof(CompleteHide), 2.0f);
    }
    
    public void CompleteHide()
    {
        _MainParticle.gameObject.SetActive(false);
        _TrailParticle.gameObject.SetActive(false);
        _Trail.enabled = false;
    }

    [ContextMenu(nameof(SetDefaultPosition))]
    public void SetDefaultPosition()
    {
        _DefaultPosition = this.transform.position;
    }
}
