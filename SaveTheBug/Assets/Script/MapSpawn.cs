﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapSpawn : MonoBehaviour
{
    public TheManager _TheManager;

    public GameObject _PrefabMapItem;

    public GameObject _MapParent;

    public int totalMapItem = 20;

    public DoorController _Door;

    public GameObject _ProcessBarParent;
    public ScrollRect _ScrollRect;
    public Image _ProcessBar;

    private void Start()
    {
        _TheManager = GameObject.Find("TheManager").GetComponent<TheManager>();
        //Spawn();
        StartCoroutine(SpawnMap());
    }

    [ContextMenu(nameof(Spawn))]
    public void Spawn()
    {
        bool revert = false;
        bool revertNextID = false;

        int sum = 0;
        int nailId = 0;

        for (int i = 0; i < totalMapItem; i++)
        {
            MapItem map = GameObject.Instantiate(_PrefabMapItem, _MapParent.transform).GetComponent<MapItem>();
            map._MapSpawn = this;

            if (revert == true)
            {
                map._MapID = sum - i;
            }
            else
            {
                map._MapID = i + 1;
            }

            if (revertNextID == true)
            {
                map._NextMapID = i - 1;
            }
            else
            {
                map._NextMapID = i + 1;
            }

            if ((i + 1) % 4 != 0)
            {

            }
            else
            {
                revert = true;
                sum = (i + 1) * 2 + 4;
                if ((i + 1) % 8 == 0)
                {
                    revert = false;
                }
            }

            if (map._MapID % 4 != 0)
            {
                map.HideTop();
                map.ShowRight();
            }
            else
            {
                map._NextMapID = i + 4;
                map.ShowTop();
                if (map._MapID % 8 != 0)
                    map.HideRight();
                else
                    revertNextID = true;
            }

            if ((map._MapID - 1) % 4 == 0)
            {
                map.HideRight();
            }
            if (i % 4 == 0)
            {
                map.ShowRight();
            }

            if (map._MapID <= _TheManager._UnlockTotal)
            {
                map.Unlock();
            }
            else
            {
                map.Lock();
            }

            map._TxtID.text = map._MapID.ToString();
        }
    }

    public IEnumerator SpawnMap()
    {
        bool revert = false;
        bool revertNextID = false;

        int sum = 0;
        int nailId = 0;

        int count = 0;

        for (int i = 0; i < totalMapItem; i++)
        {
            MapItem map = GameObject.Instantiate(_PrefabMapItem, _MapParent.transform).GetComponent<MapItem>();
            map._MapSpawn = this;

            if (revert == true)
            {
                map._MapID = sum - i;
            }
            else
            {
                map._MapID = i + 1;
            }

            if (revertNextID == true)
            {
                map._NextMapID = i - 1;
            }
            else
            {
                map._NextMapID = i + 1;
            }

            if ((i + 1) % 4 != 0)
            {

            }
            else
            {
                revert = true;
                sum = (i + 1) * 2 + 4;
                if ((i + 1) % 8 == 0)
                {
                    revert = false;
                }
            }

            if (map._MapID % 4 != 0)
            {
                map.HideTop();
                map.ShowRight();
            }
            else
            {
                map._NextMapID = i + 4;
                map.ShowTop();
                if (map._MapID % 8 != 0)
                    map.HideRight();
                else
                    revertNextID = true;
            }

            if ((map._MapID - 1) % 4 == 0)
            {
                map.HideRight();
            }
            if (i % 4 == 0)
            {
                map.ShowRight();
            }

            if (map._MapID <= _TheManager._UnlockTotal)
            {
                map.Unlock();
            }
            else
            {
                map.Lock();
            }

            if (i == totalMapItem - 1)
            {
                map.HideRight();
                map.HideTop();
            }

            map._TxtID.text = map._MapID.ToString();

            count++;
            if (count > 49)
            {
                _ProcessBar.fillAmount = (float)i / (float)totalMapItem;
                count = 0;
                yield return null;
            }
        }

        ScrollToBottom();
        _ProcessBarParent.SetActive(false);
        _Door.ToFinal();
    }

    [ContextMenu(nameof(Clear))]
    public void Clear()
    {
        int MapCount = _MapParent.transform.childCount;
        for (int i = 0; i < MapCount; i++)
        {
            DestroyImmediate(_MapParent.transform.GetChild(0).gameObject);
        }
    }

    public void GoToGame()
    {
        _Door.ToDefault();
        Invoke(nameof(ToSceneMainGame), _Door._MoveTime + 0.1f);
    }
    public void ToSceneMainGame()
    {
        _TheManager.LoadSceneMainGame();
    }

    public void ScrollToBottom()
    {
        Canvas.ForceUpdateCanvases();

        _MapParent.GetComponent<GridLayoutGroup>().CalculateLayoutInputVertical();
        _MapParent.GetComponent<ContentSizeFitter>().SetLayoutVertical();

        //_ScrollRect.content.GetComponent<VerticalLayoutGroup>().CalculateLayoutInputVertical();
        //_ScrollRect.content.GetComponent<ContentSizeFitter>().SetLayoutVertical();

        _ScrollRect.verticalNormalizedPosition = 0;
    }
}
