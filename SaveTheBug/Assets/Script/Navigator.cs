﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Navigator : MonoBehaviour
{
    public NodeManager _NodeManager;

    public List<Path> _ListPath = new List<Path>();

    //public void Start()
    //{
    //    StartFindPath();
    //}

    [ContextMenu(nameof(StartFindPath))]
    public void StartFindPath()
    {
        StartCoroutine(FindPath(_NodeManager._StartNode, _NodeManager._EndNode));
    }

    //[ContextMenu(nameof(PathRequest))]
    public void PathRequest()
    {
        _ListPath.Clear();

        Path path = new Path(_NodeManager._StartNode, _NodeManager._EndNode);
        path._Navigator = this;
        path._Id = _ListPath.Count;
        path._StartNode.AddPathID(path._Id, null);

        _ListPath.Add(path);
        DateTime time = DateTime.Now;
        for (int i = 0; i < _ListPath.Count; i++)
        {
            if ((DateTime.Now - time).Seconds > 5)
                return;

            if (_ListPath[i].SearchNode() == false)
            {
                _ListPath[i]._Stuck = true;
            }
        }

        //int pathindex = 0;
        //do
        //{
        //    if (_ListPath[pathindex].SearchNode() == false)
        //    {
        //        _ListPath[pathindex]._Stuck = true;
        //    }

        //    pathindex++;

        //    //if (_ListPath.Count <= pathindex)
        //    //{
        //    //    break;
        //    //}
        //}
        //while (_ListPath.Count > pathindex);

    }
    

    private IEnumerator FindPath(Node start, Node end)
    {
        WaitForSeconds wfs = new WaitForSeconds(0.25f);

        _ListPath.Clear();

        Path path = new Path(_NodeManager._StartNode, _NodeManager._EndNode);
        path._Navigator = this;
        path._Id = _ListPath.Count;
        path._StartNode.AddPathID(path._Id, null);

        _ListPath.Add(path);

        DateTime time = DateTime.Now;
        int count = 0;
        int i = 0;
        do /*(int i = 0; i < _ListPath.Count; i++)*/
        {
            //if ((DateTime.Now - time).Milliseconds > 100)
            //{
            //    time = DateTime.Now;
            //    yield return null;
            //}

            if (_ListPath[i].SearchNode() == false)
            {
                _ListPath[i]._Stuck = true;
                _ListPath[i].BlockSign();
            }
            else
            {
                _ListPath[i]._Stuck = false;
                _ListPath[i].SafeSign();
                i++;
                yield break;
            }

            yield return wfs;

            _ListPath[i].HideSign();
            count = _ListPath.Count - 1;
            _ListPath.RemoveAt(i);
            
        }
        while (count < _ListPath.Count) ;
    }

    public void CreateContinuePath(Path path, Node node, Node PreviousNode)
    {
        Path p = new Path(path, node);

        p._Navigator = this;

        p._Navigator = this;
        p._Id = _ListPath.Count + 1;
        p._CurrentNode.AddPathID(p._Id, PreviousNode);
        p.NormalizeIDList();

        _ListPath.Add(p);
    }

    //public void CreateTempPath()
    //{
    //    Node startNode = null;
    //    Node endNode = null;

    //    for (int i = 0; i < _ListNode.Count; i++)
    //    {
    //        if (_ListNode[i]._TempPath == true)
    //        {
    //            _ListTempPathNode.Add(_ListNode[i]);

    //            if (_ListNode[i]._StartNode == true)
    //            {
    //                startNode = _ListNode[i];
    //            }

    //            if (_ListNode[i]._EndNode == true)
    //            {
    //                endNode = _ListNode[i];
    //            }
    //        }
    //    }

    //    Path path = new Path(startNode);
    //    path.SetNodeManager(this);
    //    path.SetEndNode(endNode);
    //    path.SetId(_ListPath.Count);

    //    _ListPath = new List<Path>();
    //    _ListPath.Add(path);

    //    for (int i = 0; i < _ListPath.Count; i++)
    //    {
    //        if (_ListPath[i].SearchNodeToTempPath() == false)
    //        {
    //            _ListPath[i]._Stuck = false;
    //        }
    //        else
    //        {
    //            _ListConfirmPath.Add(_ListPath[i]);
    //        }
    //    }
    //}
}
